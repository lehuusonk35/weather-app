import React from 'react';
import { render } from '@testing-library/react';
import { BrowserRouter } from "react-router-dom";
import { act } from "react-dom/test-utils";
import HeaderSearchWeather from './index'

describe('HeaderSearchWeather Component', () => {
  test('render HeaderSearchWeather component', () => {
    const onChange = jest.fn();
    const textSearch = 'london';
    render(<BrowserRouter><HeaderSearchWeather textSearch={textSearch} onSubmitSearch={onChange} onChangeSearch={onChange}/></BrowserRouter>);
    const button = document.querySelector("[data-testid=search-button-id]");
    expect(button.textContent).toBe("Get Weather");
    act(() => {
      button.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    });
    expect(onChange).toHaveBeenCalledTimes(1);
  });
});