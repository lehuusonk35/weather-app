import React from 'react';
import {NavLink} from "react-router-dom";
import {routes} from "../../constant"
import "./headerSearchWeather.scss";

function HeaderSearchWeather({textSearch, onChangeSearch, onSubmitSearch}) {
  return (
    <div className='d-flex justify-content-between align-items-center mb-4'>
      <input type="text" className="input-search" placeholder="Search" value={textSearch} onChange={onChangeSearch} />
      <button data-testid="search-button-id" type='submit' className='btn-search btn-secondary' disabled={!textSearch} onClick={onSubmitSearch}>
        <NavLink className="dropdown-item" to={routes.detail}>Get Weather</NavLink>
      </button>
    </div>
  )
}

export default HeaderSearchWeather;