import React from 'react';
import { render, screen } from '@testing-library/react';
import Welcome from './index'

describe('Welcome Component', () => {
  test('render Welcome component', () => {
    render(<Welcome />);
    expect(screen.getByText(/Welcome!/)).toBeInTheDocument();
    expect(screen.getByText(/Please input your location to get Weather Forecast/)).toBeInTheDocument();
  });
});