import React from 'react';
import "./welcome.scss";

function Welcome() {
  return (
    <div className="welcome-content text-center">
      <h1>
        Welcome!
      </h1>
      <p>
        Please input your location to get Weather Forecast
      </p>
    </div>
  )
}

export default Welcome;