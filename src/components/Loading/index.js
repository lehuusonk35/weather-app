import React from 'react';
import "./loading.scss";

function Loading({isShowLoading}) {
    return (
        <>
            { isShowLoading && (
                <div data-testid="id-loading-app" className="spinner-loading-container spinner-loading-center-block spinner-loading-background">
                    <div className="spinner-border text-primary" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
            )}
        </>
    )
}

export default Loading;