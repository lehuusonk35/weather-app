import React from 'react';
import {render} from '@testing-library/react';
import Loading from './index';

describe('Loading component', () => {
  it('should display the loading component using snapshot testing', () => {
    const isShowLoading = true;
    const { asFragment } = render(<Loading isShowLoading={isShowLoading} />);
    expect(asFragment()).toMatchSnapshot();
  });
  test('Check Loading component must visible', () => {
    const { getByTestId } = render(<Loading  isShowLoading={true}/>);
    expect(getByTestId('id-loading-app')).toBeVisible();
  });
});