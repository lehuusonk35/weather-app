import React from 'react';

function EmptyView({message}) {
  return (
    <div className="welcome-content">
      <h1 className="display-4">
        {message}
      </h1>
    </div>
  )
}

export default EmptyView;