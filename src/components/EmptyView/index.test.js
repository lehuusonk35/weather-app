import React from 'react';
import { render } from '@testing-library/react';
import EmptyView from './index'

describe('EmptyView Component', () => {
  test('render EmptyView component', () => {
    const msg = 'Not found';
    const { getByText } = render(<EmptyView message={msg} />);
    const element = getByText('Not found');
    expect(element).toBeInTheDocument();
  });
});