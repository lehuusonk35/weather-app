import React from 'react';
import { render } from '@testing-library/react';
import ErrorAlert from './index'

describe('ErrorAlert Component', () => {
  test('render ErrorAlert component', () => {
    const msg = 'Failed to load the server-side !';
    const code = 'SERVER-ERROR'
    const { getByText } = render(<ErrorAlert errorMessage={msg} errorCode={code} />);
    const element = getByText('Failed to load the server-side !');
    expect(element).toBeInTheDocument();
  });
});