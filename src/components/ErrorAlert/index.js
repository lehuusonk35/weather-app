import React, {useEffect, useState} from 'react';
import {SERVER_ERROR} from "../../constant";
import "./errorAlert.scss"

function ErrorAlert({errorCode, errorMessage}) {
  const [isShowAlert, setIsShowAlert] = useState(false);

  useEffect(() => {
    if (errorCode === SERVER_ERROR.errorCode && errorMessage) {
      setIsShowAlert(true);
      setTimeout(() => {
        setIsShowAlert(false)
      }, 3000)
    }
  },[errorCode, errorMessage])

  return (
    <>
      {isShowAlert &&
        <div className="alert-app">
          <div className="alert alert-danger" role="alert">
            {errorMessage}
          </div>
        </div>
      }
    </>
  )
}

export default ErrorAlert;