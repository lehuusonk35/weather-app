import React, {useState} from 'react';
import {HashRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import {routes} from "../constant";
import Loading from '../components/Loading';
import ErrorAlert from "../components/ErrorAlert";
import HeaderSearchWeather from "../components/HeaderSearhWeather";
import Welcome from "../components/Welcome";
import DetailWeather from "./DeatailWeather";
import "./app.scss";
import {execGetLocation} from "../action/location";


function App() {
  const dispatch = useDispatch();
  const [textSearch, setTextSearch] = useState('');
  const isShowLoading = useSelector(state => state?.statusRequest?.isRequestRunning);
  const errorCode = useSelector(state => state?.statusRequest?.errorCode);
  const errorMessage = useSelector(state => state?.statusRequest?.errorMessage);
  const handleChangeSearch = (event) => {
    setTextSearch(event?.target?.value);
  }
  const handleSubmitSearch = () => {
    dispatch(execGetLocation(textSearch));
  }
  return (
    <Router>
      <div className="container custom-wrapper-app">
        <Loading isShowLoading={isShowLoading}/>
        <div className="row w-100">
          <div className="col custom-background p-4">
            <ErrorAlert errorCode={errorCode} errorMessage={errorMessage}/>
            <HeaderSearchWeather onChangeSearch={handleChangeSearch} textSearch={textSearch} onSubmitSearch={handleSubmitSearch}/>
            <div className="content-app">
              <Switch>
                <Route exact path={routes.home} component={Welcome} />
                <Route path={routes.detail} component={DetailWeather} />
                <Redirect to={routes.home} />
              </Switch>
            </div>
          </div>
        </div>
      </div>
    </Router>
  );
}

export default App;
