import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {ERROR_NOT_FOUND} from "../../constant";
import EmptyView from "../../components/EmptyView";
import {execGetWeather} from "../../action/weather";
import "./detailWeather.scss";

function DetailWeather() {
  const dispatch = useDispatch();
  const weatherInfo = useSelector(state => state?.weather);
  const location = useSelector(state => state?.location);
  /**
   * useEffect below will be track location.id get from the API search location.
   * If has location ID will be trigger call action to get details weather.
   */
  useEffect(() => {
    if (location.id) {
      dispatch(execGetWeather(location.id));
    }
  }, [location, dispatch])
  return (
    <>
      {!weatherInfo?.id || location.errorCode === ERROR_NOT_FOUND.errorCode ?
        <EmptyView message={location?.errorMessage}/>
        :
        <div className="detail-weather-content w-100">
          <div className="header-weather justify-content-start d-flex flex-column">
            <h2 className="display-4">{weatherInfo?.title}</h2>
            <p>{weatherInfo?.today?.date}</p>
          </div>
          <div className="main-weather mb-5">
            <div className="row align-items-center">
              <div className="col-sm-12 col-md-6 border-custom">
                <div className="row align-items-center">
                  <div className="col-5 col-md-8 wrapper-icon">
                    {weatherInfo?.today?.icon && <img alt='icon-weather' height={120} width={120} src={`https://www.metaweather.com/static/img/weather/${weatherInfo?.today?.icon}.svg`}/>}
                  </div>
                  <div className="col-7 col-md-4 justify-content-start d-flex flex-column">
                    <h2 className="display-4">{weatherInfo?.today?.currentTemp}</h2>
                    <h5>{weatherInfo?.today?.desc}</h5>
                  </div>
                </div>
              </div>
              <div className="col-sm-12 col-md-6 main-weather-detail-custom">
                <div className="row mb-3">
                  <div className="col-4">
                    <p>{weatherInfo?.today?.maxTemp}</p>
                    <p className="lead">High</p>
                  </div>
                  <div className="col-4">
                    <p>{weatherInfo?.today?.wind}</p>
                    <p className="lead">Wind</p>
                  </div>
                  <div className="col-4">
                    <p>{weatherInfo?.today?.humidity}</p>
                    <p className="lead">Humidity</p>
                  </div>
                </div>
                <div className="row">
                  <div className="col-4">
                    <p>{weatherInfo?.today?.minTemp}</p>
                    <p className="lead">Low</p>
                  </div>
                  <div className="col-4">
                    <p>{weatherInfo?.today?.pressure}</p>
                    <p className="lead">Pressure</p>
                  </div>
                  <div className="col-4">
                    <p>{weatherInfo?.today?.visibility}</p>
                    <p className="lead">Visibility</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="next-five-days">
            <h2 className="title mb-5">Daily Forecast Next 5 Days</h2>
            <div className="row justify-content-center align-items-center">
              {weatherInfo?.nextFiveDays && weatherInfo?.nextFiveDays.length > 0 && weatherInfo?.nextFiveDays.map((item) => (
                <div className="card-weather" key={item.id}>
                  <div className="card d-flex justify-content-center align-items-center flex-column p-2">
                    <h4 className="mb-0 p-2">{item?.date}</h4>
                    <img alt='icon-weather' height={40} width={40} src={`https://www.metaweather.com/static/img/weather/${item?.icon}.svg`}/>
                    <p className="lead mb-0 p-2">High: {item?.maxTemp}</p>
                    <p className="lead mb-0 p-2">Low: {item?.minTemp}</p>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      }
    </>
  )
}

export default DetailWeather;