import React from 'react';
import DetailWeather from './index';
import { mount } from 'enzyme';

// We need to wrap CommentBox with <Provider> tag in first beforeEach(() => {}) below;
// otherwise we receive this error message:
// Invariant Violation: Could not find “store” in either the context or props of “Connect(CommentBox)”
// https://stackoverflow.com/questions/36211739/invariant-violation-could-not-find-store-in-either-the-context-or-props-of-c

// The Provider's Redux store must be our own Redux store, not a mock Redux store as in comment_box.test.js because
// we insert an initial state
import { configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from 'redux';
import reducers from '../../reducers';
import {render} from "@testing-library/react";

configure({ adapter: new Adapter() });
const createStoreWithMiddleware = applyMiddleware()(createStore);

describe('DetailWeather', () => {
  let component;
  const initialState = {
    weather:
      {
        today: {
          currentTemp: `12°C`,
          minTemp: `15°C`,
          maxTemp: `10°C`,
          pressure: `1023mb`,
          humidity: `90%`,
          visibility: `$2.35miles`,
          wind: `2.4mph`,
          icon: 'cn',
          date: 'Tuesday, 3 September',
          desc: 'Cold',
        },
        nextFiveDays: [
          {
            minTemp: `16°C`,
            maxTemp: `10°C`,
            icon: 'sn',
            date: 'WED',
            id: '1223'
          },
          {
            minTemp: `12°C`,
            maxTemp: `8°C`,
            icon: 'sn',
            date: 'THU',
            id: '1226'
          },
          {
            minTemp: `18°C`,
            maxTemp: `13°C`,
            icon: 'sn',
            date: 'FRI',
            id: '1227'
          },
          {
            minTemp: `17°C`,
            maxTemp: `10°C`,
            icon: 'sn',
            date: 'SAT',
            id: '1228'
          },
          {
            minTemp: `13°C`,
            maxTemp: `4°C`,
            icon: 'cn',
            date: 'SUN',
            id: '1229'
          },
        ],
        title: 'LonDon',
        id: '1223',
      },
  };
  beforeEach(() => {
    component = mount(<Provider store={createStoreWithMiddleware(reducers, initialState)}><DetailWeather /></Provider>);
  });
  it('should display the DetailWeather component using snapshot testing', () => {
    const { asFragment } = render(<Provider store={createStoreWithMiddleware(reducers, initialState)}><DetailWeather /></Provider>);
    expect(asFragment()).toMatchSnapshot();
  });
  test('shows an card weather for next five days', () => {
    expect(component.find('.card-weather').length).toBe(5);
  });
});