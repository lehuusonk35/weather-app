import React from 'react';
import {render} from "@testing-library/react";
import { Provider } from 'react-redux';
import store from '../store';
import App from "./App";
describe('App Component', () => {
  it('should display the App component using snapshot testing', () => {
    const { asFragment } = render(<Provider store={store}>
      <App />
    </Provider>);
    expect(asFragment()).toMatchSnapshot();
  });
});