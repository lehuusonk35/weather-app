import axios from 'axios';

/**
 * Request Wrapper with default success/error actions
 */
const onSuccess = function(response) {
  console.debug('Request Successful!', response);
  return response;
};

const onError = function(error) {
  console.error('Request Failed:', error.config);

  if (error.response) {
    // Request was made but server responded with something
    // other than 2xx
    console.error('Status:',  error.response.status);
    console.error('Data:',    error.response.data);
    console.error('Headers:', error.response.headers);

  } else {
    // Something else happened while setting up the request
    // triggered the error
    console.error('EmptyView Message:', error.message);
  }

  return Promise.reject(error);
};

const get = function (url, id) {
  return axios.get(url,{
    id
  })
    .then(onSuccess)
    .catch(onError);
};

const create = function (url, requestBody) {
  return axios.post(url, {
    requestBody //request body send to sever-side must be a obj
  })
    .then(onSuccess)
    .catch(onError);
};

const update = function (url, requestBody) {
  return axios.put(url, {
    requestBody //request body send to sever-side must be a obj
  })
    .then(onSuccess)
    .catch(onError);
};

const _delete = function (url) {
  return axios.delete(url)
    .then(onSuccess)
    .catch(onError);
};

export const callApi = {
  get,
  create,
  update,
  delete: _delete
};