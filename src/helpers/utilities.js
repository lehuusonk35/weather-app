import moment from "moment";

/**
 * The handleParseWeatherData is a function convert and parse data return from the server-side
 * @param weathers: Object
 * @returns {{nextFiveDays, today: {date: string, visibility: string, maxTemp: string, icon: (string|*), humidity: string, currentTemp: string, pressure: string, minTemp: string, wind: string, desc: (string|*)}, id: (number|*), title: *}}
 */
export const handleParseWeatherData = (weathers) => {
  const listWeathers = weathers.consolidated_weather
  const today = listWeathers[0];
  const nextFiveDays = listWeathers.slice(1);
  return {
    today: {
      currentTemp: `${Math.round(today?.the_temp)}°C`,
      minTemp: `${Math.round(today?.min_temp)}°C`,
      maxTemp: `${Math.round(today?.max_temp)}°C`,
      pressure: `${Math.round(today?.air_pressure)}mb`,
      humidity: `${Math.round(today?.humidity)}%`,
      visibility: `${Math.round(today?.visibility * 10) / 10}miles`,
      wind: `${Math.round(today?.wind_speed * 100) / 100}mph`,
      icon: today?.weather_state_abbr,
      date: moment(today?.applicable_date).format("dddd, D MMMM"),
      desc: today?.weather_state_name,
    },
    nextFiveDays: nextFiveDays.map((item) => {
      return {
        minTemp: `${Math.round(item?.min_temp)}°C`,
        maxTemp: `${Math.round(item?.max_temp)}°C`,
        icon: item?.weather_state_abbr,
        date: moment(item?.applicable_date).format("ddd")?.toUpperCase(),
        id: item?.id
      }
    }),
    title:  weathers?.title,
    id:  weathers?.woeid,
  }
};