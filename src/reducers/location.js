import { createReducer } from '@reduxjs/toolkit'
import { SET_LOCATION_INFO, SET_LOCATION_ERROR } from '../action/types'

const defaultSyncData = {};

const locationReducer = createReducer(
  defaultSyncData,
  {
    [SET_LOCATION_INFO]: (state, action) => {
      if (action && action.payload) {
        return {...action.payload}; //assign new object
      }
      return state;
    },
    [SET_LOCATION_ERROR]: (state, action) => {
      if (action && action.payload) {
        return {...action.payload}; //assign new object
      }
      return state;
    }
  }
);

export default locationReducer;