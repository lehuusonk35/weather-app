import { createReducer } from '@reduxjs/toolkit';
import {ERROR_ASYNC_OP, START_ASYNC_OP, END_ASYNC_OP} from '../action/types';

const defaultSyncData = {
  isRequestRunning: false,
};

const statusRequest = createReducer(
  defaultSyncData,
  {
    [START_ASYNC_OP]: (state, action) => {
      return {
        isRequestRunning: action.payload?.isShowLoading,
      }
    },
    [END_ASYNC_OP]: () => {
      return {
        isRequestRunning: false,
      }
    },
    [ERROR_ASYNC_OP]: (state, action) => {
      if (action && action.payload) {
        return {
          ...state,
          ...action.payload
        }; //assign new object
      }
      return state;
    }
  }
);

export default statusRequest;