import { SET_LOCATION_INFO, SET_LOCATION_ERROR } from '../action/types'
import locationReducer from './location';

describe('location reducer', () => {
  const mockDataSuccess = {
    id: '1223',
  };

  const mockDataError = {
    errorCode: 'NOT_FOUND',
    errorMessage: 'Sorry, not result found !'
  };

  it('should return the initial state', () => {
    expect(locationReducer(undefined, {})).toEqual({});
  });
  it('should handle SET_LOCATION_INFO', () => {
    const successAction = {
      type: SET_LOCATION_INFO,
      payload: mockDataSuccess,
    };
    expect(locationReducer({}, successAction)).toEqual(mockDataSuccess);
  });
  it('should handle SET_LOCATION_ERROR', () => {
    const errorAction = {
      type: SET_LOCATION_ERROR,
      payload: mockDataError,
    };
    expect(locationReducer({}, errorAction)).toEqual(mockDataError);
  });
});