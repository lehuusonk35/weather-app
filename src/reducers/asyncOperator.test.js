import {START_ASYNC_OP, END_ASYNC_OP, ERROR_ASYNC_OP} from '../action/types'
import statusRequest from './asyncOperator';
import {SERVER_ERROR} from "../constant";

describe('weather reducer', () => {
  const initialState = {
    isRequestRunning: false,
  }
  it('should return the initial state', () => {
    expect(statusRequest(initialState, {})).toEqual({isRequestRunning: false,});
  });
  it('should handle START_ASYNC_OP', () => {
    const startAction = {
      type: START_ASYNC_OP,
      payload: {
        isShowLoading: true,
      },
    };
    expect(statusRequest(initialState, startAction)).toEqual({
      isRequestRunning: true,
    });
  });
  it('should handle END_ASYNC_OP', () => {
    const startAction = {
      isRequestRunning: true,
    }
    const endAction = {
      type: END_ASYNC_OP,
      payload: {},
    };
    expect(statusRequest(startAction, endAction)).toEqual({
      isRequestRunning: false,
    });
  });
  it('should handle ERROR_ASYNC_OP', () => {
    const startAction = {
      isRequestRunning: false,
    }
    const endAction = {
      type: ERROR_ASYNC_OP,
      payload: {SERVER_ERROR},
    };
    expect(statusRequest(startAction, endAction)).toEqual({
      ...startAction,
      ...endAction.payload
    });
  });
});