import { SET_WEATHER_INFO, SET_WEATHER_ERROR } from '../action/types'
import weatherReducer from './weather';

describe('weather reducer', () => {
  const mockDataSuccess = {
    today: {
      currentTemp: `12°C`,
        minTemp: `15°C`,
        maxTemp: `10°C`,
        pressure: `1023mb`,
        humidity: `90%`,
        visibility: `$2.35miles`,
        wind: `2.4mph`,
        icon: 'cn',
        date: 'Tuesday, 3 September',
        desc: 'Cold',
    },
    nextFiveDays: [
      {
        minTemp: `16°C`,
        maxTemp: `10°C`,
        icon: 'sn',
        date: 'WED',
        id: '1223'
      },
      {
        minTemp: `12°C`,
        maxTemp: `8°C`,
        icon: 'sn',
        date: 'THU',
        id: '1226'
      },
      {
        minTemp: `18°C`,
        maxTemp: `13°C`,
        icon: 'sn',
        date: 'FRI',
        id: '1227'
      },
      {
        minTemp: `17°C`,
        maxTemp: `10°C`,
        icon: 'sn',
        date: 'SAT',
        id: '1228'
      },
      {
        minTemp: `13°C`,
        maxTemp: `4°C`,
        icon: 'cn',
        date: 'SUN',
        id: '1229'
      },
    ],
      title: 'LonDon',
    id: '1223',
  };

  const mockDataError = {
    errorCode: 'NOT_FOUND',
    errorMessage: 'Sorry, not result found !'
  };

  it('should return the initial state', () => {
    expect(weatherReducer(undefined, {})).toEqual({});
  });
  it('should handle SET_WEATHER_INFO', () => {
    const successAction = {
      type: SET_WEATHER_INFO,
      payload: mockDataSuccess,
    };
    expect(weatherReducer({}, successAction)).toEqual(mockDataSuccess);
  });
  it('should handle SET_WEATHER_ERROR', () => {
    const errorAction = {
      type: SET_WEATHER_ERROR,
      payload: mockDataError,
    };
    expect(weatherReducer({}, errorAction)).toEqual(mockDataError);
  });
});