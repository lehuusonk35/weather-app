import { createReducer } from '@reduxjs/toolkit'
import { SET_WEATHER_INFO, SET_WEATHER_ERROR } from '../action/types'

const defaultSyncData = {};

const weatherReducer = createReducer(
  defaultSyncData,
  {
    [SET_WEATHER_INFO]: (state, action) => {
      if (action && action.payload) {
        return {...action.payload}; //assign new object
      }
      return state;
    },
    [SET_WEATHER_ERROR]: (state, action) => {
      if (action && action.payload) {
        return {...action.payload}; //assign new object
      }
      return state;
    }
  }
);

export default weatherReducer;