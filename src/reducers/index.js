import { combineReducers } from 'redux';
import statusRequest from './asyncOperator';
import location from './location';
import weather from './weather';

export default combineReducers({location, weather, statusRequest});
