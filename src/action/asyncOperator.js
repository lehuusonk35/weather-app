import {createAction} from '@reduxjs/toolkit';
import {START_ASYNC_OP, END_ASYNC_OP, ERROR_ASYNC_OP} from './types';
import {SERVER_ERROR} from "../constant";

export const startAsync = createAction(START_ASYNC_OP);
export const endAsync = createAction(END_ASYNC_OP);
export const errorAsync = createAction(ERROR_ASYNC_OP);

/**
 * wrapAsyncOperation is a function wrapper all promise will be call in this app
 * In this function can control state start/end/error/success/finally of all Promise
 * Expected: All dispatch Action to call a Promise in the project must use
 * @param wrapped
 * @param isShowLoading: if param isShowLoading = true, will be show Loading. If isShowLoading = false, just call a API not show loading
 * @returns {function(*): Q.Promise<any>}
 */
export const wrapAsyncOperation = (wrapped, {isShowLoading = true} = {}) =>
  dispatch => {
    dispatch(startAsync({ isShowLoading}));
    return dispatch(wrapped)
      .then((result) => {
        return result;
      })
      .catch((error) => {
        dispatch(errorAsync({SERVER_ERROR}));
        return Promise.reject(error.message)
      }).finally(()=> {
        dispatch(endAsync());
      })
  };
