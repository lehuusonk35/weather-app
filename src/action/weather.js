import {createAction} from '@reduxjs/toolkit';
import {wrapAsyncOperation} from "./asyncOperator";
import {callApi} from '../helpers/handleWrapperPromise';
import {SET_WEATHER_INFO, SET_WEATHER_ERROR} from './types';
import {GET_WEATHER_URL, SERVER_ERROR} from '../constant';
import {handleParseWeatherData} from "../helpers/utilities";

export const setWeatherInfoAction = createAction(SET_WEATHER_INFO);
export const setWeatherErrorAction = createAction(SET_WEATHER_ERROR);

/**
 * getWeatherProcess is a function handle to dispatch action handle action set state of weather reducer
 * @param woeId: number
 * @returns {(function(*): Promise<void>)|*}
 */
const getWeatherProcess = (woeId) => {
  return async dispatch => {
    try {
      const weatherInfo = await getWeatherDetail(woeId);
      dispatch(setWeatherInfoAction(weatherInfo));
    }
    catch (e) {
      dispatch(setWeatherErrorAction(SERVER_ERROR));
      throw e;
    }
  }
};

/**
 * getWeatherDetail is a funtion to fetch data get weather forecast use location id from location reducer
 * @param name: string
 * @returns {Promise<{nextFiveDays, today: {date: string, visibility: string, maxTemp: string, icon: (string|*), humidity: string, currentTemp: string, pressure: string, minTemp: string, wind: string, desc: (string|*)}, id: (number|*), title: *}>}
 */
const getWeatherDetail = async (name) => {
  const weather = await callApi.get(GET_WEATHER_URL + name);
  const listWeathers = weather?.data;
  return handleParseWeatherData(listWeathers);
};


export const execGetWeather = (woeId) => wrapAsyncOperation(getWeatherProcess(woeId));