import configureStore from 'redux-mock-store'
import { getDefaultMiddleware } from '@reduxjs/toolkit';
import * as actions from './weather';
import axios from 'axios';
import {SERVER_ERROR} from "../constant";
import {handleParseWeatherData} from "../helpers/utilities";
import {endAsync, startAsync} from "./asyncOperator";
jest.mock('axios');

const middlewares = getDefaultMiddleware({
  serializableCheck: false,
});
const mockStore = configureStore(middlewares);

describe('actions', () => {
  describe('execGetWeather', () => {
    it('should dispatch execGetWeather success', async () => {
      const weatherData = {
        "consolidated_weather":[
          {
            "id":6248924265316352,
            "weather_state_name":"Heavy Cloud",
            "weather_state_abbr":"hc",
            "wind_direction_compass":"W",
            "created":"2021-09-11T06:59:02.771498Z",
            "applicable_date":"2021-09-11",
            "min_temp":16.509999999999998,
            "max_temp":22.31,
            "the_temp":22.115000000000002,
            "wind_speed":6.2104993684853795,
            "wind_direction":260.1925380267736,
            "air_pressure":1016.5,
            "humidity":63,
            "visibility":10.981803766006522,
            "predictability":71
          },
          {
            "id":5666263901667328,
            "weather_state_name":"Heavy Cloud",
            "weather_state_abbr":"hc",
            "wind_direction_compass":"NNW",
            "created":"2021-09-11T06:59:02.892459Z",
            "applicable_date":"2021-09-12",
            "min_temp":14.114999999999998,
            "max_temp":21.83,
            "the_temp":20.405,
            "wind_speed":3.546282335798935,
            "wind_direction":345.46434335993035,
            "air_pressure":1019.0,
            "humidity":60,
            "visibility":11.631136661894535,
            "predictability":71
          },
          {
            "id":5594983953334272,
            "weather_state_name":"Showers",
            "weather_state_abbr":"s",
            "wind_direction_compass":"E",
            "created":"2021-09-11T06:59:02.154893Z",
            "applicable_date":"2021-09-13",
            "min_temp":13.059999999999999,
            "max_temp":18.785,
            "the_temp":18.87,
            "wind_speed":6.116701842206845,
            "wind_direction":92.07418995768698,
            "air_pressure":1017.5,
            "humidity":67,
            "visibility":9.666050266443968,
            "predictability":73
          },
          {
            "id":5887694564491264,
            "weather_state_name":"Light Rain",
            "weather_state_abbr":"lr",
            "wind_direction_compass":"NE",
            "created":"2021-09-11T06:59:02.944490Z",
            "applicable_date":"2021-09-14",
            "min_temp":13.7,
            "max_temp":19.125,
            "the_temp":19.32,
            "wind_speed":5.293349827203797,
            "wind_direction":55.34875302427638,
            "air_pressure":1013.5,
            "humidity":73,
            "visibility":10.518260856597472,
            "predictability":75
          },
          {
            "id":5804269119733760,
            "weather_state_name":"Heavy Cloud",
            "weather_state_abbr":"hc",
            "wind_direction_compass":"NE",
            "created":"2021-09-11T06:59:02.367324Z",
            "applicable_date":"2021-09-15",
            "min_temp":13.885,
            "max_temp":21.4,
            "the_temp":22.380000000000003,
            "wind_speed":4.149745901079032,
            "wind_direction":42.98738543394577,
            "air_pressure":1015.0,
            "humidity":68,
            "visibility":13.199477551101566,
            "predictability":71
          },
          {
            "id":5570039722803200,
            "weather_state_name":"Heavy Cloud",
            "weather_state_abbr":"hc",
            "wind_direction_compass":"E",
            "created":"2021-09-11T06:59:04.367390Z",
            "applicable_date":"2021-09-16",
            "min_temp":13.77,
            "max_temp":24.35,
            "the_temp":24.0,
            "wind_speed":2.585157480314961,
            "wind_direction":94.50000000000001,
            "air_pressure":1018.0,
            "humidity":64,
            "visibility":9.999726596675416,
            "predictability":71
          }
        ],
        "time":"2021-09-11T09:42:00.196657+01:00",
        "sun_rise":"2021-09-11T06:29:11.984153+01:00",
        "sun_set":"2021-09-11T19:23:51.111113+01:00",
        "timezone_name":"LMT",
        "parent":{
          "title":"England",
          "location_type":"Region / State / Province",
          "woeid":24554868,
          "latt_long":"52.883560,-1.974060"
        },
        "sources":[
          {
            "title":"BBC",
            "slug":"bbc",
            "url":"http://www.bbc.co.uk/weather/",
            "crawl_rate":360
          },
          {
            "title":"Forecast.io",
            "slug":"forecast-io",
            "url":"http://forecast.io/",
            "crawl_rate":480
          },
          {
            "title":"HAMweather",
            "slug":"hamweather",
            "url":"http://www.hamweather.com/",
            "crawl_rate":360
          },
          {
            "title":"Met Office",
            "slug":"met-office",
            "url":"http://www.metoffice.gov.uk/",
            "crawl_rate":180
          },
          {
            "title":"OpenWeatherMap",
            "slug":"openweathermap",
            "url":"http://openweathermap.org/",
            "crawl_rate":360
          },
          {
            "title":"Weather Underground",
            "slug":"wunderground",
            "url":"https://www.wunderground.com/?apiref=fc30dc3cd224e19b",
            "crawl_rate":720
          },
          {
            "title":"World Weather Online",
            "slug":"world-weather-online",
            "url":"http://www.worldweatheronline.com/",
            "crawl_rate":360
          }
        ],
        "title":"London",
        "location_type":"City",
        "woeid":44418,
        "latt_long":"51.506321,-0.12714",
        "timezone":"Europe/London"
      };
      const resp = {data: weatherData};
      const mockState = {};
      const store = mockStore(mockState);
      const expectedActions = [
        startAsync({isShowLoading: true}),
        actions.setWeatherInfoAction(handleParseWeatherData(weatherData)),
        endAsync(),
      ];
      axios.get.mockResolvedValue(resp);
      store.dispatch(actions.execGetWeather('44418')).then(() => {
        //Check dispatch action setWeatherErrorAction must be call
        expect(store.getActions()).toEqual(expectedActions);
        done();
      });
    });
    it('should dispatch execGetWeather error', async () => {
      const mockState = {};
      const store = mockStore(mockState);
      const expectedActions = [
        actions.setWeatherErrorAction(SERVER_ERROR),
      ];
      axios.get.mockRejectedValue({message: 'Error'});
      store.dispatch(actions.execGetWeather()).then(() => {
        //Check dispatch action setWeatherErrorAction must be call
        expect(store.getActions()).toContainEqual(expectedActions);
        done();
      });
    });
  });
});