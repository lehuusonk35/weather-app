import {createAction} from '@reduxjs/toolkit';
import {wrapAsyncOperation} from "./asyncOperator";
import {callApi} from '../helpers/handleWrapperPromise';
import {SET_LOCATION_INFO, SET_LOCATION_ERROR} from './types';
import {GET_LOCATION_URL, ERROR_NOT_FOUND, SERVER_ERROR} from '../constant';

export const setLocationInfoAction = createAction(SET_LOCATION_INFO);
export const setLocationErrorAction = createAction(SET_LOCATION_ERROR);

/**
 * getWeatherProcess is a function to dispatch action handle action set state of location reducer
 * @param search: string
 * @returns {(function(*): Promise<>)|*}
 */
const getWeatherProcess = (search) => {
  return async dispatch => {
    try {
      const woeId = await getLocation(search);
      if(woeId) {
       return dispatch(setLocationInfoAction({id: woeId}));
      }
      return dispatch(setLocationErrorAction(ERROR_NOT_FOUND));
    }
    catch (e) {
      dispatch(setLocationErrorAction(SERVER_ERROR));
      throw e;
    }
  }
};

/**
 * getLocation is a function to fetch data for search location
 * @param name:string
 * @returns {Promise<>}
 */
const getLocation = async (name) => {
  const location = await callApi.get(GET_LOCATION_URL + name);
  return location.data[0]?.woeid;
}

export const execGetLocation = (search) => wrapAsyncOperation(getWeatherProcess(search));