export const START_ASYNC_OP = 'START_ASYNC_OP';
export const END_ASYNC_OP = 'END_ASYNC_OP';
export const ERROR_ASYNC_OP = 'ERROR_ASYNC_OP';


export const SET_LOCATION_INFO = 'SET_LOCATION_INFO';
export const SET_LOCATION_ERROR = 'SET_LOCATION_ERROR';

export const SET_WEATHER_INFO = 'SET_WEATHER_INFO';
export const SET_WEATHER_ERROR = 'SET_WEATHER_ERROR';