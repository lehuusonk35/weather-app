import configureStore from 'redux-mock-store'
import { getDefaultMiddleware } from '@reduxjs/toolkit';
import * as actions from './location';
import {startAsync, endAsync} from './asyncOperator';
import axios from 'axios';
import {SERVER_ERROR} from "../constant";
jest.mock('axios');

const middlewares = getDefaultMiddleware({
  serializableCheck: false,
});
const mockStore = configureStore(middlewares);

describe('actions', () => {
  describe('execGetWeather', () => {
    it('should dispatch execGetLocation success', async () => {
      const locationData = [
        {
          "title":"London",
          "location_type":"City",
          "woeid":44418,
          "latt_long":"51.506321,-0.12714"
        }
      ];
      const resp = {data: locationData};
      const mockState = {};
      const store = mockStore(mockState);
      const expectedActions = [
        startAsync({isShowLoading: true}),
        actions.setLocationInfoAction({id: 44418}),
        endAsync(),
      ];
      axios.get.mockResolvedValue(resp);
      store.dispatch(actions.execGetLocation('london')).then(() => {
        //Check dispatch action setLocationInfoAction must be call
        expect(store.getActions()).toEqual(expectedActions);
        done();
      });
    });
    it('should dispatch execGetLocation error', async () => {
      const mockState = {};
      const store = mockStore(mockState);
      const expectedActions = [
        actions.setLocationErrorAction(SERVER_ERROR),
      ];
      axios.get.mockRejectedValue({message: 'Error'});
      store.dispatch(actions.execGetLocation()).then(() => {
        //Check dispatch action setLocationErrorAction must be call
        expect(store.getActions()).toContainEqual(expectedActions);
        done();
      });
    });
  });
});