export const GET_LOCATION_URL = 'https://thingproxy.freeboard.io/fetch/https://www.metaweather.com/api/location/search/?query=';
export const GET_WEATHER_URL = 'https://thingproxy.freeboard.io/fetch/https://www.metaweather.com/api/location/';

export const ERROR_NOT_FOUND = {
  errorCode: 'NOT_FOUND',
  errorMessage: 'Sorry, not result found !'
}

export const SERVER_ERROR = {
  errorCode: 'SERVER-ERROR',
  errorMessage: 'Failed to load from server !'
}

export const routes = {
  home: '/',
  detail: '/detail'
}