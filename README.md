# Weather App

<p>The Weather App is a simple application to test join a new project.<br>
This Application provides Users can search locations and display a daily weather forecast in the selected city.<br>
This project was bootstrapped with Create React App.<br>
<p>Below you will find some information on how to perform common tasks.</p>
<h2><a id="user-content-table-of-contents" class="anchor" aria-hidden="true" href="#table-of-contents"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Table of Contents</h2>
<ul>
    <li><a href="#how-to-install">How to install</a></li>
    <li><a href="#getting-started">Getting Started</a></li>
    <li><a href="#folder-structure">Folder Structure</a></li>
    <li><a href="#functionality-overview">Functionality overview</a></li>
    <li><a href="#run-unit-test">Run Unit Test</a></li>
    <li><a href="#built-with">Built with</a></li>
</ul>
<h2><a class="anchor" aria-hidden="true" href="#how-to-install"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>How to install</h2>
<p>Clone repository and go to the project folder</p>
<pre>git clone https://gitlab.com/lehuusonk35/weather-app.git
<span class="pl-c1">cd</span> weather-app</pre>
<p>Then you should install required dependencies.</p>
<pre>yarn install</pre>
<h2><a class="anchor" aria-hidden="true" href="#getting-started"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Getting started</h2>
<p>To launch project you need to execute following command:</p>
<pre>yarn start</pre>
<p>Open in browser: <a href="http://localhost:8888" rel="nofollow">http://localhost:3000</a></p>
<h2><a class="anchor" aria-hidden="true" href="#folder-structure"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Folder Structure</h2>
<div class="snippet-clipboard-content position-relative"><pre><code>react-redux-app-template/
|
├──public/                              // Directory for the build
|  ├──icons                             // Icons for the manifest.json and favicon
|  ├──favicon.ico
|  └──manifest.json                     // The web app manifest is a simple JSON file
|                                       // that tells the browser about your web application 
|                                       // and how it should behave when 'installed' 
|                                       // on the user's mobile device or desktop.
|                      
├──src/                                   
|   ├──actions                          // Redux actions
│   │   ├──[name].js                            
│   │   ├──[Name].test.js               // Jest test
│   │   └──types.js                     // Redux action type constants     
│   │
|   ├──components                       // Components that are reused                
│   │   ├──[Name] 
│   │   │   ├──index.js
│   │   │   ├──index.test.js           // Jest test 
│   │   │   ├──[Name].scss             // Components style (optional) 
│   │   │   └───[__snapshots__]
│   │   │       └──index.test.js.snap  //Jest snapshots test
│   │   └──...   
│   │  
|   ├──containers                       // Components that use redux connect (Containers)
│   │   ├──[Name] 
│   │   │   ├──[Name].js
│   │   │   ├──[Name].test.js           // Jest test 
│   │   │   ├──*[Name].scss             // Container styles (optional)   
│   │   │   └───[__snapshots__]
│   │   │       └──index.test.js.snap  //Jest snapshots test
│   │   └──...                                         
│   │
|   ├──helpers                          // Functions that are reused 
│   │   └──[Name].js 
│   │
|   ├──reducers                         // Reducers
│   │   ├──[name].js  
│   │   ├──[name].test.js               // Jest test
│   │   ├──...  
│   │   └──index.js                     // combineReducers
│   │
|   ├──styles                           
│   │   ├──main.scss                    // Import modules scss use in app (example: bootstrap, reset.local.scss,...)
│   │   ├──reset.local.scss             // Rest style of some properties element use
│   │   └──typography.scss              // Basic styles, setup px/rem
│   │   
|   ├──constant.js                      // Constant config, routes, api,...
|   ├──index.js                         // App entry
|   └──store.js                         // createStore 
|                                     
└──package.json                    // defines functional attributes of a project that npm uses to install dependencies, run scripts, and identify the entry point to our package.
</code></pre></div>
<h2><a class="anchor" aria-hidden="true" href="#functionality-overview"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Functionality overview</h2>
<p><strong>General functionality:</strong></p>
<ul>
    <li>Weather feature:
        <ul>
            <li>Search the weather by location, display today and next five days weather forecast</li>
            <li>Handle empty state if search not found</li>
            <li>Handle error if failed to load from the server-side</li>
            <li>Reducer to keep your connectivity state in the Redux store</li>
            <li>Responsive web page</li>
        </ul>
    </li>
    <li>Component:
        <ul>
            <li>HeaderSearchWeather: handle input search text by Users.</li>
            <li>EmptyView: display after can't get weather response</li>
            <li>ErrorAlert: display after failed to load from the server-side</li>
            <li>Loading: display while loading data from the server-side</li>
        </ul>
    </li>
</ul>
<p><strong>The general page breakdown looks like this:</strong></p>
<ul>
    <li>Home page (URL: /#/ )</li>
    <li>Detail Weather page (URL: /#/detail )
    <ul>
        <li>Today weather forecast and weather details</li>
        <li>List next five days weather forecast</li>
    </ul>
    </li>
</ul>
<p><strong>API:</strong></p>
<ul>
    <li>Search location: https://thingproxy.freeboard.io/fetch/https://www.metaweather.com/api/location/search/?query=location
    <ul>
        <li>This API provides search method to query location ID by a string</li>
        <li>Return a data response include woeid and basic location information</li>
    </ul>
    </li>
    <li>Search weather: https://thingproxy.freeboard.io/fetch/https://www.metaweather.com/api/location/woeid
    <ul>
        <li>Using woeid from the response of Search location return to query weather forecast</li>
        <li>Return a data response include location information and weather forecast of this location</li>
    </ul>   
    </li>
</ul>
<p><strong>Using Redux: In project has three reducers</strong></p>
<ul>
    <li>asyncOperator:
    <ul>
        <li>Handle wrapper all Promise, handle start/end/error state of a Promise.</li>
        <li>Return state to display Loading component, ErrorAlert component.</li>
    </ul>
    </li>
    <li>location:
    <ul>
        <li>Handle fetch data get location by string.</li>
        <li>Return state to update location id</li>
    </ul>   
    </li>
    <li>weather:
    <ul>
        <li>Handle fetch data get weather by woeid get from location reducer.</li>
        <li>Return state to update the weather forecast</li>
    </ul>   
    </li>
</ul>
<h2><a class="anchor" aria-hidden="true" href="#run-unit-test"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Run unit test</h2>
<p>In this project use Jest, Enzyme to test renderer, snapshots for Component and Action Creators, Reducers for Redux.</p>
<p>Launches the test runner in the interactive watch mode:</p>
<pre>yarn test</pre>
<h2><a class="anchor" aria-hidden="true" href="#built-with"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z"></path></svg></a>Built with</h2>
<ul>
<li><a href="https://www.javascript.com/" rel="nofollow">JavaScript</a> JavaScript allows the programmers to build large-scale web application easily.</li>
<li><a href="https://facebook.github.io/react/docs/hello-world.html" rel="nofollow">React</a> A javascript library for building user interfaces.</li>
<li><a href="http://redux.js.org/" rel="nofollow">Redux</a> is a predictable state container for JavaScript apps.</li>
<li><a href="https://getbootstrap.com/" rel="nofollow">Bootstrap</a> The world’s most popular front-end open source toolkit.</li>
<li><a href="https://github.com/reactjs/react-redux">react-redux</a> Official React bindings for Redux.</li>
<li><a href="https://github.com/gaearon/redux-thunk">redux-thunk</a> Redux Thunk middleware allows you to write action creators that return a function instead of an action. The thunk can be used to delay the dispatch of an action, or to dispatch only if a certain condition is met. The inner function receives the store methods dispatch and getState as parameters.</li>
<li><a href="https://reactrouter.com/web/guides/quick-start">React Router</a> for routing website location</li>
<li><a href="http://sass-lang.com/" rel="nofollow">Sass</a> CSS with superpowers. Sass boasts more features and abilities than any other CSS extension language out there.</li>
<li><a href="https://github.com/facebook/create-react-app">create-react-app</a> Create React App is a tool built by developers at Facebook to help you build React applications. It saves you from time-consuming setup and configuration. You simply run one command and create react app sets up the tools you need to start your React project.</li>
<li><a href="http://jestjs.io/docs/en/expect.html#content">Jest</a> Jest is a delightful JavaScript Testing Framework with a focus on simplicity</li>
<li><a href="https://airbnb.io/enzyme/docs/api/">Enzyme </a> Enzyme is a JavaScript Testing utility for React that makes it easier to test your React Components' output.</li>
</ul>